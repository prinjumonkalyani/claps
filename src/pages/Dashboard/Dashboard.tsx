/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { DashboardWrapper } from "./styled";

const Dashboard = () => {
  return (
    <DashboardWrapper>
      <header className="aui-main-header aui-pri-header">
        <a href="www.heart.org" className="aui-skip-content">
          Skip to main content
        </a>
        <nav className="navbar navbar-expand-lg justify-content-between aui-header-content mx-auto aui-pri-header-t">
          <a href="/" className="claps-logo" aria-label="Claps Logo" />
          <button
            className="navbar-toggler ml-2 px-0"
            type="button"
            data-toggle="collapse"
            data-target="#toggleNav"
            aria-controls="toggleNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="aha-icon-hamburger" />
          </button>
          <div
            className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
            id="toggleNav"
          >
            <ul className="navbar-nav mx-lg-3 flex-lg-row flex-column">
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/">Home</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/rewards">Rewards</a>
                </button>
              </li>
              <li className="d-flex nav-item dropdown px-lg-3 flex-column">
                <button
                  type="button"
                  className="btn btn-text dropdown-toggle nav-link"
                  id="navDropdown1"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  My Account
                </button>
                <div
                  className="dropdown-menu p-lg-0 aui-header-dropdown"
                  aria-labelledby="navDropdown1"
                  role="menu"
                >
                  <ul>
                    <li className="active">
                      <a className="dropdown-item py-2" href="www.heart.org">
                        Settings
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item py-2" href="www.heart.org">
                        Sign Out
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <div className="d-flex flex-column flex-grow-1">
        <div className="bgcolor d-flex justify-content-between">
          <h1>Employee Dashboard</h1>
          <Button variant="contained" color="default">
            <Link to="/createreward">Create Reward</Link>
          </Button>
        </div>
        <div className="d-lg-flex container-fluid">
          <div className="aui-sidenav">
            <div className="navbar-expand-lg overflow-hidden">
              <button
                className="navbar-toggler float-right m-3"
                type="button"
                data-toggle="collapse"
                data-target="#sideNavbar"
                aria-controls="sideNavbar"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="aha-icon-hamburger-round" />
              </button>
              <div className="text-center m-3">
                <img src="../../images/userdash.png" alt="User" />
                <h2 className="pt-3">Employee Name</h2>
                <p className="pt-3">ID: 22343</p>
              </div>
            </div>
          </div>
          <div className="rewards-section ml-4 flex-grow-1">
            <div className="row row-cols-1 row-cols-md-2 pt-5">
              <div className="col mb-4">
                <div className="card">
                  <img
                    src="../images/user.png"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h5 className="card-title">Best Performer of the Year</h5>
                    <p className="card-text">
                      This is the award for the best performer who is XYZ for
                      his exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
              <div className="col mb-4">
                <div className="card">
                  <img
                    src="../images/user.png"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h5 className="card-title">Best Team</h5>
                    <p className="card-text">
                      This is the award for the best Team who are XYZ for his
                      exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
              <div className="col mb-4">
                <div className="card">
                  <img
                    src="../images/user.png"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h5 className="card-title">Best Employeer of the Month</h5>
                    <p className="card-text">
                      This is the award for the best Employeer who is XYZ for
                      his exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
              <div className="col mb-4">
                <div className="card">
                  <img
                    src="../images/user.png"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h5 className="card-title">Best New Joiner</h5>
                    <p className="card-text">
                      This is the award for the best New Joiner who is XYZ for
                      his exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
            </div>
            {/* <div className="row pt-4">
              <div className="col-lg-6 d-flex justify-content-between r1">
                <div className="col-lg-3">
                  <img src="../images/trophy.png" alt="best" />
                </div>
                <div className="col-lg-3">
                  <h3 className="text-center">Best Performer of the year</h3>
                </div>
              </div>
              <div className="col-lg-6">
                <h3 className="r2 text-center">Best Team</h3>
              </div>
            </div> */}
            {/* <div className="row pt-5">
              <div className="col-lg-6">
                <h3 className="r3 text-center">Employeer of the month</h3>
              </div>
              <div className="col-lg-6">
                <h3 className="r4 text-center">Best New Joiner</h3>
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </DashboardWrapper>
  );
};

export default Dashboard;
