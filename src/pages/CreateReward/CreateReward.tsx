/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { CreateRewardWrapper } from "./styled";

const CreateReward = () => {
  return (
    <CreateRewardWrapper>
      <header className="aui-main-header aui-pri-header">
        <a href="www.heart.org" className="aui-skip-content">
          Skip to main content
        </a>
        <nav className="navbar navbar-expand-lg justify-content-between aui-header-content mx-auto aui-pri-header-t">
          <a href="/" className="claps-logo" aria-label="Claps Logo" />
          <button
            className="navbar-toggler ml-2 px-0"
            type="button"
            data-toggle="collapse"
            data-target="#toggleNav"
            aria-controls="toggleNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="aha-icon-hamburger" />
          </button>
          <div
            className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
            id="toggleNav"
          >
            <ul className="navbar-nav mx-lg-3 flex-lg-row flex-column">
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/">Home</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/rewards">Rewards</a>
                </button>
              </li>
              <li className="d-flex nav-item dropdown px-lg-3 flex-column">
                <button
                  type="button"
                  className="btn btn-text dropdown-toggle nav-link"
                  id="navDropdown1"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  My Account
                </button>
                <div
                  className="dropdown-menu p-lg-0 aui-header-dropdown"
                  aria-labelledby="navDropdown1"
                  role="menu"
                >
                  <ul>
                    <li className="active">
                      <a className="dropdown-item py-2" href="www.heart.org">
                        Settings
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item py-2" href="www.heart.org">
                        Sign Out
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <div className="d-flex flex-column flex-grow-1">
        <div className="bgcolor d-flex justify-content-between">
          <h1>Create Reward For Employee</h1>
        </div>
        <div className="d-lg-flex container-fluid">
          <div className="aui-sidenav">
            <div className="navbar-expand-lg overflow-hidden">
              <button
                className="navbar-toggler float-right m-3"
                type="button"
                data-toggle="collapse"
                data-target="#sideNavbar"
                aria-controls="sideNavbar"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="aha-icon-hamburger-round" />
              </button>
              <div className="text-center m-3">
                <img src="../../images/userdash.png" alt="User" />
                <h2 className="pt-3">Employee Name</h2>
                <p className="pt-3">ID: 22343</p>
              </div>
            </div>
          </div>
          <div className="createreward-section pt-4 ml-4 flex-grow-1">
            <form>
              <div className="form-group row required">
                <label htmlFor="name" className="col-sm-4 col-form-label">
                  First Name
                </label>
                <div className="col-sm-8">
                  <input
                    type="text"
                    className="form-control"
                    id="name"
                    required
                  />
                </div>
              </div>
              <div className="form-group row required">
                <label htmlFor="lastname" className="col-sm-4 col-form-label">
                  Last Name
                </label>
                <div className="col-sm-8">
                  <input
                    type="text"
                    className="form-control"
                    id="lastname"
                    required
                  />
                </div>
              </div>
              <div className="form-group row required">
                <label htmlFor="id" className="col-sm-4 col-form-label">
                  Employee ID
                </label>
                <div className="col-sm-8">
                  <input
                    type="number"
                    className="form-control"
                    id="id"
                    required
                  />
                </div>
              </div>
              <div className="form-group row required">
                <label htmlFor="type" className="col-sm-4 col-form-label">
                  Department
                </label>
                <div className="col-sm-8">
                  <input
                    type="text"
                    className="form-control"
                    id="type"
                    required
                  />
                </div>
              </div>
              <div className="form-group row required">
                <label htmlFor="award" className="col-sm-4 col-form-label">
                  Team/Employee Receiving the Award
                </label>
                <div className="col-sm-8">
                  <input
                    type="text"
                    className="form-control"
                    id="award"
                    required
                  />
                </div>
              </div>
              <div className="form-group row required">
                <label htmlFor="id" className="col-sm-4 col-form-label">
                  Number of Nominations Allowed
                </label>
                <div className="col-sm-8">
                  <input
                    type="number"
                    className="form-control"
                    id="id"
                    required
                  />
                </div>
              </div>
              <div className="form-group row required">
                <label htmlFor="id" className="col-sm-4 col-form-label">
                  Nomination Start Date
                </label>
                <div className="col-sm-8">
                  <input
                    type="date"
                    className="form-control"
                    id="id"
                    required
                  />
                </div>
              </div>
              <div className="form-group row required">
                <label htmlFor="id" className="col-sm-4 col-form-label">
                  Nomination End Date
                </label>
                <div className="col-sm-8">
                  <input
                    type="date"
                    className="form-control"
                    id="id"
                    required
                  />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="checkbox" className="col-sm-4 col-form-label">
                  Type of Reward
                </label>
                <div className="col-sm-8 d-flex pt-2 justify-content-around">
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio1"
                      value="option1"
                      checked
                    />
                    <label htmlFor="radio1">Spot</label>
                  </div>
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio2"
                      value="option2"
                    />
                    <label htmlFor="radio2">Nomination</label>
                  </div>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="checkbox" className="col-sm-4 col-form-label">
                  Reward to be given to:
                </label>
                <div className="col-sm-8 d-flex pt-2 justify-content-around">
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio3"
                      value="option3"
                      checked
                    />
                    <label htmlFor="radio3">Team</label>
                  </div>
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio4"
                      value="option4"
                    />
                    <label htmlFor="radio4">Individual</label>
                  </div>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="checkbox" className="col-sm-4 col-form-label">
                  Nomination Type:
                </label>
                <div className="col-sm-8 d-flex pt-2 justify-content-around">
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio5"
                      value="option5"
                      checked
                    />
                    <label htmlFor="radio5">Public</label>
                  </div>
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio6"
                      value="option6"
                    />
                    <label htmlFor="radio6">Panel</label>
                  </div>
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="checkbox" className="col-sm-4 col-form-label">
                  Auto Publish
                </label>
                <div className="col-sm-8 d-flex pt-2 justify-content-around">
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio7"
                      value="option7"
                      checked
                    />
                    <label htmlFor="radio7">True</label>
                  </div>
                  <div className="form-group form-radio">
                    <input
                      type="radio"
                      name="radio"
                      id="radio8"
                      value="option8"
                    />
                    <label htmlFor="radio8">False</label>
                  </div>
                </div>
              </div>
            </form>
            <div className="buttonstyle">
              <Button variant="contained" color="default">
                Create Reward
              </Button>
              <Button variant="contained" color="default">
                Cancel
              </Button>
            </div>
          </div>
        </div>
      </div>
    </CreateRewardWrapper>
  );
};

export default CreateReward;
