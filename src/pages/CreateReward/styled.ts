/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const CreateRewardWrapper = styled.div`
  .bgcolor {
    background: linear-gradient(
      140deg,
      #be10e1 0%,
      #bc70a4 50%,
      #d641bd 75%
    ) !important;
    padding: 10px;
  }
  .bgcolor h1 {
    color: #fff;
    font-family: "Allison", cursive;
    font-size: 65px;
  }
  .claps-logo {
    background: url(../images/logo.png) no-repeat;
    background-size: contain;
    width: 20rem;
    min-height: 5.5625rem;
    position: relative;
    color: #222328;
  }
  .claps-logo:before {
    position: absolute;
    width: 0.0625rem;
    height: 5.5625rem;
    background-color: #e3e3e3;
    left: 6.375rem;
    top: 0;
    content: "";
  }
  .claps-logo:after {
    content: "Claps";
    font-size: 1.375rem;
    font-weight: 600;
    position: absolute;
    left: 6.875rem;
    top: 1.8125rem;
  }
  .aui-sidenav {
    height: 100vh;
  }
  .MuiButton-contained {
    background: linear-gradient(
      140deg,
      #be10e1 0%,
      #bc70a4 50%,
      #d641bd 75%
    ) !important;
  }
  .buttonstyle {
    text-align: center;
  }
  .buttonstyle button {
    margin: 10px;
  }
  a {
    color: #000;
    text-decoration: none;
  }
`;
